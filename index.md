---
layout: default
e-commerce-1: Tooso provides
e-commerce-2: AI to transform eCommerce search
e-commerce-3: into a personalized and interactive experience.
button-text: Request a Demo
desc-1: Why isn't your search engine as smart as your shopping assistant?
desc-2: Tooso tailors the search experience around your customers to understand their
  needs and bring them to check out.
trusted-by: Trusted by industry leaders
trusted-logos:
- "/assets/logo-liu-jo.svg"
- "/assets/logo-canali.svg"
- "/assets/logo-leroy-merlin.svg"
- "/assets/logo-arduino.svg"
experience-main: Give your clients unprecedented user experience
experience-blocks:
- image: "/assets/icon-conversion-rate.svg"
  title: Increase your conversion rate
  description: Guide your customers from search to checkout&#58 our clients saw a
    conversion increase up to 20%.
- image: "/assets/icon-ux.svg"
  title: Turn one-time visitors into loyal customers
  description: Personalized search UX treats customers as unique. Make customers feel
    at home and they will always come back for more.
- image: "/assets/icon-focus.svg"
  title: Focus on what matters
  description: Free your team from the manual search maintenance&#58 let Tooso take
    care of it automatically, with AI.
experience-secondary: Why are you underserving your clients?
experience-impact:
- percentage: 80%
  description: of customers that had a bad search experience leave your shop
- percentage: 65%
  description: of customers that search for something require further interactions
    to find what they want
tooso-difference: Why Tooso makes the difference
steps:
- Search and discovery
- Understand users intentions
- Speak or type
- Personalize the user experience
step-1-text1: Wedding Suit
step-1-text2: Which color?
step-1-description: Tooso provides an interactive search experience by asking relevant
  questions to your customers, leading them to products they want to buy.
step-2-description: Tooso identifies the most relevant items and allows customers
  to describe the products by brand, color, material, style, occasion and much more.
step-2-words:
- text: sleeveless
  details: "#type"
- text: red
  details: "#color"
- text: cocktail
  details: "#occasion"
- text: dress
  details: "#object"
- text: 100-200 $
  details: "#price range"
step-3-description: Tooso is designed for a great UX on every device and supports
  speech recognition for vocal search.
step-3-search-bar: red cocktail dress
step-4-p1-name: Laura
step-4-p2-name: Rachael
step-4-p1-text: bought an elegant dress for a cocktail party
step-4-p2-text: bought a pair of yoga pants to jog in the park
step-4-description: Tooso learns your customers' preferences from their behavior and
  their shopping history, so they will always get the most relevant search results.
team: Our Team
team-members:
- name: Jacopo Tagliabue
  job: CTO
  description: Educated in several acronyms worldwide (UNISR, SFI, MIT) and former
    lead data scientist at Axon Vibe, Jacopo is Tooso's CTO.
- name: Ciro Greco
  job: CEO
  description: Ph.D. in Linguistics and Neuroscience (Milan-Bicocca/MIT) and former
    post-doctoral fellow at Ghent University, Ciro is Tooso’s CEO.
- name: Mattia Pavoni
  job: COO
  description: Digital business consultant and strategist for more than 5 years (focus
    on eCommerce), Mattia is Tooso's COO.
review-talk: They talk about us
review-text: "“ Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam iaculis
  ligula quis augue malesuada tincidunt. Integer nisi tellus, venenatis hendrerit
  turpis non, sodales interdum sapien. Etiam ut gravida turpis, at tristique arcu.
  Mauris in mattis orci. Donec pharetra molestie erat. “"
map-address-1: "<strong>Tooso Inc.</strong><br/>c/o RocketSpace<br/>180 Sansome St<br/>San
  Francisco<br/>CA, 94104<br/>+1 415 37 64 178"
map-address-2: "<strong>Tooso Italia</strong><br/>Via dei Pellegrini, 18<br/>Milano<br/>MI,
  20122<br/>+39 02 8295 2527"
footer-text1: Tooso Inc. • Copyright 2017 Tooso Inc.
---

