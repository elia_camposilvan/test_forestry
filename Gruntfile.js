module.exports = function(grunt) {
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    babel: {
      options: {
        sourceMap: true,
        presets: ['env']
      },
      build: {
        files: [
          {
            expand: false,
            src: ['_js/*.js'],
            dest: '_tmp/js/bundle.js'
          }
        ]
      }
    },
    uglify: {
      options: {
        banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n'
      },
      build: {
        src: ['_tmp/js/bundle.js'],
        dest: 'assets/bundle.min.js'
      }
    },
    watch: {
      scripts: {
        files: ['_js/*.js'],
        tasks: ['babel', 'uglify'],
        options: {
          spawn: false,
        },
      },
    },
  });

  grunt.loadNpmTasks('grunt-babel');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.registerTask('default', ['babel', 'uglify', 'watch']);
};